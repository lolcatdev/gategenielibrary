/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gategenie.bl;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author francesca
 */
public class User {
    protected String name;
    protected String lastName;
    protected String id;
    protected String email;
    protected String address;
    protected String nationality;
    protected LocalDate birthday;
    protected String age;
    
    //Constructores
    public User(){
    }

    public User(String name, String lastName, String id, String email, String address, String nationality, LocalDate birthday, String age) {
        this.name = name;
        this.lastName = lastName;
        this.id = id;
        this.email = email;
        this.address = address;
        this.nationality = nationality;
        this.birthday = birthday;
        this.age = age;
    }
    
    //ToString

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", lastName=" + lastName + ", id=" + id + ", email=" + email + ", address=" + address + ", nationality=" + nationality + ", birthday=" + birthday + ", age=" + age + '}';
    }
    
    //Getters & Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    //Equals

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.nationality, other.nationality)) {
            return false;
        }
        if (!Objects.equals(this.age, other.age)) {
            return false;
        }
        if (!Objects.equals(this.birthday, other.birthday)) {
            return false;
        }
        return true;
    }
    
    
}
